import pygame
import pymunk
import math
from entities import Entity, Rope
from images import LoadAnimation, RenderAnimation
from enum import Enum

class GrabbingState(Enum):
    MOVING = 1
    DESCENDING = 2
    ASCENDING = 3
    RETURNING = 4

class ClawCrane(Entity):
    def __init__(self, initial_pos, minX, maxX, tiledir, segmentLength, ropeTile):
        self.initial_pos = pymunk.Vec2d(initial_pos)
        self.position = pymunk.Vec2d(initial_pos)
        self.range = (minX, maxX)
        self.target_velocity = 0
        self.imgs = LoadAnimation(tiledir) 
        self.rope = Rope(2, segmentLength, ropeTile)
        self.state = GrabbingState.MOVING
        self.counter = 0

    def addToPhysics(self, space):
        self.body = pymunk.Body(0,0, pymunk.Body.KINEMATIC)
        self.body.position = self.position
        self.body.velocity = (0,0) 
        self.shape = pymunk.Poly.create_box(self.body, size=(4, 4))
        space.add(self.body, self.shape)

        self.rope.anchor(self.body)

    def move(self, direction):
        if self.state == GrabbingState.MOVING: 
            self.target_velocity = direction

    def cancel_move(self):
        if self.state == GrabbingState.MOVING: 
            self.target_velocity = 0

    def grab(self):
        if self.state == GrabbingState.MOVING: 
            self.target_velocity = 0
            self.state = GrabbingState.DESCENDING 
        
    def tick(self, level):
        self.counter += 1
        posPrevious = self.position.x
        self.position.x += self.target_velocity
        self.position.x = min(max(self.position.x,self.range[0]), self.range[1])
        self.body.position = self.position
        self.body.velocity = (self.position.x-posPrevious, 0)

        if self.state == GrabbingState.DESCENDING:
            self.rope.extend()
            self.rope.attemptGrab()
            if self.rope.attached:
                self.state = GrabbingState.ASCENDING
                target = self.rope.grabbedBody()
                if target != None and target.body_type != pymunk.Body.DYNAMIC:
                    self.rope.release()

        if self.state == GrabbingState.ASCENDING:
            if self.counter % 4 == 0:
                self.rope.retract()
            if self.rope.length() <= 2:
                self.state = GrabbingState.RETURNING

        if self.state == GrabbingState.RETURNING:
            self.target_velocity = math.copysign( 0.5,
                self.initial_pos.x - self.position.x)
            if math.fabs(self.initial_pos.x - self.position.x) < 2:
                self.target_velocity = 0
                self.state = GrabbingState.MOVING
                self.rope.release()

    def render(self, display):
        RenderAnimation(display, self.imgs, self.position, -self.position.x*32)
