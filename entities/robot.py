import pygame
import pymunk
import math
from entities import Entity
from images import LoadAnimationDictionary, RenderAnimationDictionary

class Robot(Entity):
    def __init__(self, position, size, tiledir):
        self.position = position
        self.size = size
        self.tiledir = tiledir
        self.grounded = False
        self.imgs = LoadAnimationDictionary(tiledir, 
            ["active", "inactive"])
        self.target_velocity = 0
        self.console = None
        self.time = 0;
        self.on = False 

    def addToPhysics(self, space):
        self.space = space
        self.body = pymunk.Body(10, pymunk.inf)
        self.body.position = self.position
        self.shape = pymunk.Poly.create_box(self.body, size=self.size)
        self.shape.friction = 1.0
        self.feet = pymunk.Poly(self.body, [
                (-self.size[0]/2, self.size[1]/2),
                (self.size[0]/2, self.size[1]/2),
                (self.size[0]/2, self.size[1]/2+1),
                (-self.size[0]/2, self.size[1]/2+1)
                ])
        space.add(self.body, self.shape, self.feet)


    def tick(self, level):
        self.on = True
        if self.console != None:
            if not self.console.available(level):
                self.on = False

        grounding = {
            'normal' : pymunk.Vec2d.zero(),
            'penetration' : pymunk.Vec2d.zero(),
            'impulse' : pymunk.Vec2d.zero(),
            'position' : pymunk.Vec2d.zero(),
            'body' : None
        }
        def f(arbiter):
            n = -arbiter.contact_point_set.normal
            if n.y < grounding['normal'].y:
                grounding['normal'] = n
                grounding['penetration'] = -arbiter.contact_point_set.points[0].distance
                grounding['body'] = arbiter.shapes[1].body
                grounding['impulse'] = arbiter.total_impulse
                grounding['position'] = arbiter.contact_point_set.points[0].point_b
        self.body.each_arbiter(f)

        if grounding['body'] != None and abs(grounding['normal'].x/grounding['normal'].y) < 2.0:
            self.grounded = True
        else: 
            self.grounded = False

        if self.grounded:
            self.feet.friction = 2000/self.space.gravity.y
            self.feet.surface_velocity = -self.target_velocity, 0
        else:
            self.feet.friction = 0.0
        if(self.target_velocity != 0):
            self.time += math.copysign(30, self.target_velocity)

    def move(self, direction):
        self.target_velocity = direction

    def cancel_move(self):
        self.target_velocity = 0

    def render(self, display):
        state = "inactive"
        if self.on:
            state = "active"
        
        RenderAnimationDictionary(display, self.imgs, 
            (self.body.position[0], self.body.position[1]), state, self.time)
