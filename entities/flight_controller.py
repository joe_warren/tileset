#!/usr/bin/env python3
from entities import Entity
import pymunk

class FlightController(Entity):
    def __init__(self, robot, targetPosition, forceVector, d=0.5, i=0.01):
        self.robot = robot
        self.targetPosition = pymunk.Vec2d(targetPosition)
        self.forceVector = pymunk.Vec2d(forceVector)
        self.forceNormalized = self.forceVector.normalized()
        self.forceTangent = self.forceNormalized.perpendicular()
        self.d = d
        self.i = i
        self.integral = 0.0
    def tick(self, level):
        displacement = self.robot.body.position - self.targetPosition
        
        p = displacement.dot(self.forceNormalized)
        d = self.robot.body.velocity.dot(self.forceNormalized)
        d = d * self.d
        self.integral += p
        i = self.integral * self.i
        tangent = self.robot.target_velocity.dot(self.forceTangent)
        self.robot.target_velocity = tangent * self.forceTangent 
        self.robot.target_velocity -= (p+i+d)*self.forceVector

