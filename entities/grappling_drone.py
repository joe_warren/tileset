import pymunk
from entities import Drone, Rope


class GrapplingDrone(Drone):
    def __init__(self, position, size, tiledir, segmentLength, ropeTile ):
        super().__init__(position, size, tiledir)
        self.rope = Rope(0, segmentLength, ropeTile)
        self.decending = False

    def addToPhysics(self, space):
        super().addToPhysics(space)
        self.rope.anchor(self.body)

    def toggle(self):
        self.decending = not self.decending

    def tick(self, level):
        super().tick(level)
        if self.decending:
            self.rope.extend()
            self.rope.attemptGrab()
        else:
            self.rope.release()
            self.rope.retract()

    
            


