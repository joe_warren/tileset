import pymunk
import pygame
from entities import Entity
from images import LoadAnimation, RenderTransition, FRAMERATE

class Door(Entity):
    def __init__(self, pos, size, tiledir):
        self.imgs = LoadAnimation(tiledir)
        self.lastChanged = 0
        self.pos = pos
        self.size = size
        self.closed = True
        self.controls = {}

    def available(self, level):
        return False

    def tick(self, level):
        pass

    def setLastChanged(self):
        animTime = (1000/FRAMERATE) * len(self.imgs)
        time = pygame.time.get_ticks()
        delta = time - self.lastChanged
        self.lastChanged = time - max(0, animTime - delta) 

    def open(self, level):
        if(self.closed):
            self.setLastChanged()
            self.body.position = (0,0)
            self.closed = False

    def close(self, level):
        if(not self.closed):
            self.setLastChanged()
            self.body.position = self.pos
            self.closed = True

    def addToPhysics(self, space):
        self.body = pymunk.Body(0, 0, pymunk.Body.KINEMATIC)
        self.body.position = self.pos
        self.shape = pymunk.Poly.create_box(self.body, size=self.size)
        self.shape.friction = 1.0
        space.add(self.body, self.shape)

    def render(self, display):
        RenderTransition(display, self.imgs, self.pos, self.lastChanged, self.closed)
