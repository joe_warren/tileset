import pymunk
from entities import Entity

class Patroller(Entity):
    def __init__(self, robot, velocity):
        self.robot = robot
        self.direction = 1.0
        self.velocity = velocity
        self.robot.move(self.direction * self.velocity)
    def tick(self, level):
        width = self.robot.shape.bb.right - self.robot.shape.bb.left
        nextPosition = self.robot.body.position + pymunk.vec2d.Vec2d(
            (width/2.0 + 4)*self.direction, 0
        )
        pq = self.robot.space.point_query_nearest(nextPosition, 
            0.0, pymunk.ShapeFilter())

        if pq != None:
            self.direction = -self.direction
            self.robot.cancel_move()
            self.robot.move(self.direction * self.velocity)
            
        
