import pygame
import pymunk
from entities import Entity

class Sensor(Entity):
    def __init__(self, pos, size):
        self.pos = pos
        self.size = size
        self.active = False
        self.controls = {}

    def recalculateShape(self):
        self.sensor_body.position = self.pos
        self.sensor_shape.unsafe_set_vertices(
            pymunk.Poly.create_box(
                self.sensor_body, 
                size=self.size).get_vertices())

    def addToPhysics(self, space):
        self.sensor_body = pymunk.Body(0, 0, pymunk.Body.KINEMATIC)
        self.sensor_body.position = self.pos
        self.sensor_shape = pymunk.Poly.create_box(
                self.sensor_body, 
                size=self.size)
        self.sensor_shape.sensor = True
        space.add(self.sensor_body, self.sensor_shape)

    def available(self, level):
        if self.sensor_shape.point_query(level.player.body.position)[0] < 0:
            return True
        return False
