from images import LoadAnimationDictionary, RenderAnimationDictionary

class Lamp():
    def __init__(self, position, tiledir):
        self.position = position
        self.tiledir = tiledir
        self.imgs = LoadAnimationDictionary(tiledir, ["on", "off"])
        self.state = "off"
    def on(self, level):
        self.state = "on"
    def off(self, level):
        self.state = "off"

    def available(self, level):
        return False
    def addToPhysics(self, space):
        pass
    def tick(self, level):
        pass
    def render(self, display):
        RenderAnimationDictionary(display, self.imgs, self.position, self.state)
