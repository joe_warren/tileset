import pymunk
import pygame
import math
from images import Render, RenderAnimationDictionary, LoadAnimationDictionary
from entities import Entity

class Rope(Entity):
    def __init__(self, segmentCount, segmentLength, tilePath):
        self.segmentCount = segmentCount
        self.segmentLength = segmentLength
        self.mass = 0.01
        self.links = []
        self.imgs = LoadAnimationDictionary(tilePath, 
            ["link", "attached", "detached"])
        self.attached = False
        self.attachmentConstraint = None
        self.preattachedMass = 0
        self.preattachedMoment = 0

    def addToPhysics(self, space):
        self.space = space

    def anchor(self, body):
        self.root = body
        prevBody = body 
        for i in range(0, self.segmentCount):
            self.extend()
            
    def extend(self):
        if self.attached:
            # only extend the rope when we're not holding onto anything
            return
        r = self.segmentLength/3.0

        #previous body is the last link in the chain, or the root if the chain 
        # is empty
        prevBody = self.root
        if len(self.links) != 0:
            prevBody = self.links[-1][0]

        #offset the pin if we're attaching to the root
        offset = 10 if len(self.links) == 0 else 0
        nextPosition = (prevBody.position[0], 
            prevBody.position[1] + self.segmentLength + offset)

        #check to see if there's space for the new joint
        pq = self.space.point_query_nearest(nextPosition, 
                    0.0, pymunk.ShapeFilter())
        if pq == None:
            # if there is, create a new link
            moment = pymunk.moment_for_circle(self.mass, 0, r)
            nextBody = pymunk.Body(self.mass, moment)
            nextBody.position = nextPosition
            nextShape = pymunk.Circle(nextBody, r)
            prevAnchor = (0, 0)
            if len(self.links) == 0:
                prevAnchor = (0, 10)
            joint = pymunk.constraint.SlideJoint(prevBody, nextBody, 
                prevAnchor, (0, 0), 0, self.segmentLength)
            self.space.add(nextBody, nextShape, joint)
            self.links.append((nextBody, nextShape, joint))
            
    def retract(self):
        if len(self.links) > 0:
            (body, shape, joint) = self.links.pop()
            self.space.remove(body, shape, joint)
            if self.attached and len(self.links) > 0:
                (newend, a, b) = self.links[-1]
                targetBody = self.attachmentConstraint.b
                targetAnchor = self.attachmentConstraint.anchor_b
                distance = self.attachmentConstraint.distance
                self.space.remove(self.attachmentConstraint)
                self.attachmentConstraint = pymunk.constraint.PinJoint(
                    newend, targetBody, (0, 0), targetAnchor)
                self.attachmentConstraint.distance = distance
                self.space.add(self.attachmentConstraint)
                targetBody.apply_impulse_at_local_point((0, -5))

    def attemptGrab(self):
        if not self.attached and len(self.links) > 0:
            body, shape, joint = self.links[-1]
            grabPosition = (body.position[0], 
                body.position[1] + self.segmentLength)
            pq = self.space.point_query_nearest(grabPosition, 
                    0.0, pymunk.ShapeFilter())

            if pq != None:
                self.attached = True
                targetBody = pq.shape.body
                pointOnTarget = targetBody.world_to_local(grabPosition)
                self.attachmentConstraint = pymunk.constraint.PinJoint(
                    body, targetBody, (0, 0), pointOnTarget)
                self.space.add(self.attachmentConstraint)
                if targetBody.body_type == pymunk.Body.DYNAMIC:
                    self.preattachedMass = targetBody.mass
                    self.preattachedMoment = targetBody.moment
                    targetBody.mass = 0.1
                    if targetBody.moment != pymunk.inf:
                        bb = pq.shape.bb
                        size = (bb.right-bb.left, bb.top-bb.bottom)
                        targetBody.moment = pymunk.moment_for_box(
                            targetBody.mass, size
                        )
    def length(self):
        return len(self.links)
                
    def grabbedBody(self):
        if not self.attached:
            return None
        return self.attachmentConstraint.b

    def release(self):
        if self.attached:
            body = self.attachmentConstraint.b
            if body.body_type == pymunk.Body.DYNAMIC:
                body.mass = self.preattachedMass
                body.moment = self.preattachedMoment
            self.space.remove(self.attachmentConstraint)
            self.attachmentConstraint = None
            self.attached = False

    def _joint_angle(self, joint):
        a = joint.a.position + joint.anchor_a
        b = joint.b.position + joint.anchor_b
        v = b-a
        return 90-v.get_angle_degrees()

    def render(self, surface):
        if len(self.links) > 0: 
            *start, last = self.links 
            for body, shape, joint in start:
                RenderAnimationDictionary(surface, self.imgs, body.position, "link")
            body, shape, joint = last
            if self.attached:
                joint = self.attachmentConstraint
            angle = self._joint_angle(joint)

            state = "attached" if self.attached else "detached"
            frame = int(round(angle/10)) % len(self.imgs[state])
            Render(surface, self.imgs[state][frame], body.position)
            
