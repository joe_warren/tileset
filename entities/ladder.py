import pygame
import pymunk
from entities import Sensor

class Ladder(Sensor):
    def __init__(self, x, y_top, y_bottom, filepath):
        self.x = x
        self.y_top = y_top
        self.y_bottom = y_bottom
        self.img =  pygame.image.load(filepath)
        self.active = False

        super().__init__((x, y_top + y_bottom/2), (24, y_bottom - y_top)) 

        self.controls = {
            pygame.K_a: (
                lambda x: self.move_up(x), 
                lambda x: self.stop_move(x),
                "climb"),
            pygame.K_z: (
                lambda x: self.move_down(x), 
                lambda x: self.stop_move(x),
                "descend")
        }
    def recalculateShape(self):
        self.pos = (self.x, self.y_top + self.y_bottom/2)
        self.size = (24, self.y_bottom - self.y_top)
        super().recalculateShape()

    def addToPhysics(self, space):
        super().addToPhysics(space)
        self.platform_body = pymunk.Body(0,0, pymunk.Body.KINEMATIC)
        self.platform_body.position = (0,0)
        self.platform_body.velocity = (0,0) 
        self.platform_shape = pymunk.Poly.create_box(
                self.platform_body,
                size=(24, 4)
                )
        self.platform_shape.friction = 1.0
        space.add(self.platform_body, self.platform_shape)

    def activate(self, level):
        self.active = True
        level.player.climbing = True
        self.platform_body.position = (float(self.x), 
                float(level.player.body.position.y + 24/2 + 2))

    def deactivate(self, level):
        self.active = False
        level.player.climbing = False
        self.platform_body.position = (0.0, 0.0)
        self.platform_body.velocity = (0.0, 0.0)

    def move_up(self, level):
        self.activate(level)
        self.platform_body.velocity = (0.0,-1.0) 
    def move_down(self, level):
        self.activate(level)
        self.platform_body.velocity = (0.0,1.0) 
    def stop_move(self, level):
        self.platform_body.velocity = (0.0,0.0) 

    def tick(self, level):
        if self.active:
            self.platform_body.position += self.platform_body.velocity

            if self.platform_body.position.y < self.y_top + 24 + 6:
                self.platform_body.position = (
                        self.platform_body.position.x, self.y_top + 24 + 6)

            displacement = self.platform_body.position.y - 24.0/2 - 2.0 - level.player.body.position.y
            if (not self.available(level)) or displacement > 4 or displacement < -24:
                self.deactivate(level)

    def render(self, display):
        height = self.img.get_height()
        for y in range(self.y_bottom, self.y_top, -height):
            display.blit(self.img, (self.x - 16/2, y-height))
