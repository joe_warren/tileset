import pymunk
from entities import Robot
from entities import Boost

class BoostRobot(Robot):
    def __init__(self, position, size, robotTiledir, impulse, boostTiledir):
        super().__init__(position, size, robotTiledir)
        self.boost = Boost(
            pymunk.vec2d.Vec2d(position) + pymunk.vec2d.Vec2d(0, -size[1]+1),
            impulse, 
            boostTiledir
        )
    def addToPhysics(self, space):
        super().addToPhysics(space)
        self.boost.addToPhysics(space)

    def tick(self, level):
        super().tick(level)
        pos = self.body.position + pymunk.vec2d.Vec2d(0, -self.size[1]+1)
        self.boost.pos = pos
        self.boost.sensor_body.position = pos
        self.boost.tick(level)

    def render(self, display):
        super().render(display)
        self.boost.render(display)
