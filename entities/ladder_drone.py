import pymunk
from entities import Drone, Ladder

class LadderDrone(Drone):
    def __init__(self, position, size, tiledir, laddertile):
        super().__init__(position, size, tiledir)
        self.ladder = Ladder(0, 0, 0, laddertile)
        self.retracting = False
      
    def toggle(self):
        if self.active:
            self.active = False
            self.ladder.x = int(self.body.position.x)
            self.ladder.y_top = int(self.body.position.y)
            self.ladder.y_bottom = int(self.body.position.y+16)
            self.ladder.recalculateShape()
        else:
            self.retracting = True

    def tick(self, level):
        super().tick(level)
        if not self.active:
            if self.retracting:
                self.ladder.y_bottom -= 4
                if self.ladder.y_bottom < self.ladder.y_top:
                    self.active = True
                    self.retracting = False
                    self.ladder.y_bottom = 0
                    self.ladder.y_top = 0
                    self.ladder.x = 0
                self.ladder.recalculateShape()
            else:
                pq = level.space.point_query_nearest((self.ladder.x, self.ladder.y_bottom+4),
                    0.0, pymunk.ShapeFilter())
                if pq == None:
                    self.ladder.y_bottom += 2
                    self.ladder.recalculateShape()
            


