import pygame 
import pymunk
from entities import Sensor
from images import LoadAnimation, RenderTransition

class Boost(Sensor):
    def __init__(self, position, impulse, tiledir):
        self.tiledir = tiledir
        self.impulse = impulse

        self.imgs = LoadAnimation(tiledir)

        self.lastHit = 0

        super().__init__(position, (32, 32))

    def tick(self, level):
        time = pygame.time.get_ticks()
        if self.available(level) and time - self.lastHit > 500:
            self.lastHit = time
            impulse = (self.impulse[0] + level.player.target_velocity/10, self.impulse[1])
            level.player.body.apply_impulse_at_local_point(impulse)

    def render(self, display):
        RenderTransition(display, self.imgs, self.pos, self.lastHit)


        
