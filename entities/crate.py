import math
import pymunk
from entities import Entity
from images import LoadAnimation, Render


class Crate(Entity):
    def __init__(self, position, size, mass, friction, tiledir):
        self.tiles = LoadAnimation(tiledir)
        self.position = position
        self.size = size
        self.mass = mass
        self.friction = friction

    def addToPhysics(self, space):
        self.space = space
        moment = pymunk.moment_for_box(self.mass, self.size)
        self.body = pymunk.Body(self.mass, moment)
        self.body.position = self.position
        self.body.angle = 30
        self.shape = pymunk.Poly.create_box(self.body, size=self.size)
        self.shape.friction = self.friction
        self.shape.elasticity = 0.1
        space.add(self.body, self.shape)
    def render(self, display):
        frame = int(round(math.degrees(self.body.angle)/10)) % len(self.tiles)
        Render(display, self.tiles[frame], self.body.position)


