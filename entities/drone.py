import pygame
import pymunk
import math
from entities import Entity
from images import LoadAnimationDictionary, RenderAnimationDictionary

class Drone(Entity):
    def __init__(self, position, size, tiledir, mass=1.0):
        self.position = position
        self.mass=mass
        self.size = size
        self.tiledir = tiledir
        self.grounded = False
        self.imgs = LoadAnimationDictionary(tiledir, 
            ["hover", "grounded", "left", "right"])
        self.target_velocity = pymunk.Vec2d(0, 0)
        self.active = True

    def addToPhysics(self, space):
        self.space = space
        self.body = pymunk.Body(self.mass, pymunk.inf)
        self.body.position = self.position
        self.body.force = (0,-1024*self.mass)
        self.shape = pymunk.Poly.create_box(self.body, size=self.size)
        self.shape.elasticity = 0.5
        self.shape.friction = 0.5
        space.add(self.body, self.shape)

    def tick(self, level):
        grounding = {
            'normal' : pymunk.Vec2d.zero(),
            'penetration' : pymunk.Vec2d.zero(),
            'impulse' : pymunk.Vec2d.zero(),
            'position' : pymunk.Vec2d.zero(),
            'body' : None
        }
        def f(arbiter):
            n = -arbiter.contact_point_set.normal
            if n.y < grounding['normal'].y:
                grounding['normal'] = n
                grounding['penetration'] = -arbiter.contact_point_set.points[0].distance
                grounding['body'] = arbiter.shapes[1].body
                grounding['impulse'] = arbiter.total_impulse
                grounding['position'] = arbiter.contact_point_set.points[0].point_b
        self.body.each_arbiter(f)

        if grounding['body'] != None and abs(grounding['normal'].x/grounding['normal'].y) < 2.0:
            self.grounded = True
        else: 
            self.grounded = False
        if self.active:
            # target velocity
            self.body.force = self.target_velocity
            # plus lift to counter gravity
            self.body.force += (0, -1024*self.mass)
            # plus air resistance
            self.body.force += self.body.velocity * -0.5
        else:
            self.body.velocity = (0, 0)
    
    def move(self, direction):
        self.target_velocity += direction

    def cancel_move(self):
        self.target_velocity = pymunk.Vec2d(0,0)

    def render(self, display):
        state = "hover"
        if self.grounded:
            state="grounded"
        elif self.target_velocity.x < -0.5:
            state="left"
        elif self.target_velocity.x > 0.5: 
            state="right"
        RenderAnimationDictionary(display, self.imgs, 
            (self.body.position[0], self.body.position[1]), state)
