import pygame 
import pymunk
from entities import Sensor
from images import LoadAnimationDictionary, RenderAnimationDictionary

class Console(Sensor):
    def __init__(self, position, tiledir):
        self.tiledir = tiledir
        self.imgs = LoadAnimationDictionary(tiledir, ["active", "inactive"])

        self.lastHit = 0
        self.state = "inactive"
        super().__init__(position, (32, 32))

    def tick(self, level):
        if self.available(level):
            self.state = "active"
        else:
            self.state = "inactive"
    def render(self, display):
        RenderAnimationDictionary(display, self.imgs, self.pos, self.state)
