import pygame
from entities import Console
def Exit(position, tiledir):
    exit = Console(position, tiledir) 
    exit.controls = {pygame.K_a:
            (lambda s: s.success(), lambda x: x, "Exit")
    }
    return exit
