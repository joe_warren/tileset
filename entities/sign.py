import pygame
from entities import Console

def Sign(position, tiledir, avatar, message):
    sign = Console(position, tiledir) 
    sign.controls = {pygame.K_a:
        (lambda s: s.addMessage(avatar, message), lambda x: x, "Read")
    }
    return sign
