import pygame
import pymunk
from images import LoadAnimationDictionary, RenderAnimationDictionary

class Player():
    def __init__(self, position, tiledir):
        self.position = position
        self.tiledir = tiledir
        self.imgs = LoadAnimationDictionary(tiledir, 
            ["right", "left", "standing", "jumping", "falling", "hanging", "climbing/still", "climbing/up", "climbing/down"])
        self.grounded = False
        self.climbing = False
        self.target_velocity = 0

        self.controls = {
             pygame.K_UP: (lambda x: self.jump(), lambda x: x),
             pygame.K_LEFT: (lambda x: self.move(-512), lambda x: self.cancel_move()),
             pygame.K_RIGHT: (lambda x: self.move(512), lambda x: self.cancel_move())
        }

    def available(self, level):
        return True

    def addToPhysics(self, space):
        self.space = space
        self.body = pymunk.Body(1, pymunk.inf)
        self.body.position = self.position
        self.shape = pymunk.Poly.create_box(self.body, size=(16,24))
        space.add(self.body, self.shape)
        self.grounding = {
            'normal' : pymunk.Vec2d.zero(),
            'penetration' : pymunk.Vec2d.zero(),
            'impulse' : pymunk.Vec2d.zero(),
            'position' : pymunk.Vec2d.zero(),
            'body' : None
        }

    def tick(self, level):
        self.grounding = {
            'normal' : pymunk.Vec2d.zero(),
            'penetration' : pymunk.Vec2d.zero(),
            'impulse' : pymunk.Vec2d.zero(),
            'position' : pymunk.Vec2d.zero(),
            'body' : None
        }

        def f(arbiter):
            n = -arbiter.contact_point_set.normal
            if n.y < self.grounding['normal'].y:
                self.grounding['normal'] = n
                self.grounding['penetration'] = -arbiter.contact_point_set.points[0].distance
                self.grounding['body'] = arbiter.shapes[1].body
                self.grounding['impulse'] = arbiter.total_impulse
                self.grounding['position'] = arbiter.contact_point_set.points[0].point_b
        self.body.each_arbiter(f)

        if self.grounding['body'] != None and abs(self.grounding['normal'].x/self.grounding['normal'].y) < 2.0:
            self.grounded = True
        else: 
            self.grounded = False

        if self.grounded: 
            self.shape.friction = 4000/self.space.gravity.y
            self.shape.surface_velocity = -self.target_velocity, 0
        else:
            self.shape.friction = 0.0

    def jump(self):
        if self.grounded:
            self.body.apply_impulse_at_local_point((0,-256))
            if self.grounding["body"] != None :
                self.grounding["body"].apply_impulse_at_local_point((0,256))

    def move(self, direction):
        if not self.grounded:
            self.body.apply_impulse_at_local_point((direction/10,0))
        self.target_velocity = direction

    def cancel_move(self):
        self.target_velocity = 0

    def render(self, display):
        if self.climbing:
            if self.body.velocity.y < 0:
                state = "climbing/up"
            elif self.body.velocity.y > 0:
                state = "climbing/down"
            else:
                state = "climbing/still"
        elif self.grounded:
            if self.target_velocity < 0:
                state = "left"
            elif self.target_velocity > 0:
                state = "right"
            else:
                state = "standing"
        else:
            if self.body.velocity.x < -0.1:
                state = "left"
            elif self.body.velocity.x > 0.1:
                state = "right"
            elif self.body.velocity.y < -35:
                state = "jumping"
            elif self.body.velocity.y > 35:
                state = "falling"
            else:
                state = "hanging"

        RenderAnimationDictionary(display, self.imgs, 
            (self.body.position[0], self.body.position[1]-5), state)
