import pygame
from os import path
from game_grid import TILESIZE
from random import randrange
class MenuBackground():
    def __init__(self, title, tiledir):
        self.tiledir = tiledir
        tiles = {
            (True, True, True, True): "x",
            (True, False, True, True): "w",
            (False, True, True, True): "m",
            (True, True, False, True): "t",
            (True, True, True, False): "d",
            (True, True, False, False): "l",
            (False, False, True, True): "-",
            (True, False, True, False): "]",
            (True, False, False, True): "[",
            (False, True, False, True): "{",
            (False, True, True, False): "}",
            (False, True, False, False): "l",
            (True, False, False, False): "l",
            (False, False, False, True): "-",
            (False, False, True, False): "-",
        }
        self.tiles = {k: pygame.image.load(self._nameOfTileFile(v)) 
                for k, v in tiles.items()}
        self.emptyTile = pygame.image.load(self._nameOfTileFile("_"))
        self.title = pygame.image.load(title)
        self.grid = [[]]
    def width(self):
        if self.height() == 0:
            return 0
        return len(self.grid[0])

    def height(self):
        return len(self.grid)

    def _nameOfTileFile(self, tile):
        return path.join(self.tiledir, tile + ".png")

    def _tileMap(self):
        def at(x, y):
            if x<0 or y< 0 or x >=self.width() or y >= self.height():
                return False
            else:
                return self.grid[y][x]
        def lookup(x,y):
            if not self.grid[y][x]:
                return self.emptyTile
            else:
                k = (at(x, y-1), at(x, y+1), at(x-1, y), at(x+1, y))
                return self.tiles[k]
        return ((lookup(x, y) for x in range(0, self.width()))
            for y in range(0,self.height()))
    
    def regenerateGrid(self, w, h):
        self.grid = [[False for _ in range(0, w)] for __ in range(0, h)]
        for n in range(0, 3):
            y = randrange(0, self.height())
            for x in range(0, self.width()):
                if self.grid[y][x]:
                    break  
                self.grid[y][x] = True

            x = randrange(0, self.width())
            for y in range(0, self.height()):
                if self.grid[y][x]:
                    break  
                self.grid[y][x] = True

            y = randrange(0, self.height())
            for x in range(self.width()-1, -1, -1):
                if self.grid[y][x]:
                    break  
                self.grid[y][x] = True

            x = randrange(0, self.width())
            for y in range(self.height()-1, -1, -1):
                if self.grid[y][x]:
                    break  
                self.grid[y][x] = True

    def render(self, display):
        w = int(display.get_width()/TILESIZE)+1
        h = int(display.get_height()/TILESIZE)+1
        if self.height() != h or self.width() != w:
            self.regenerateGrid(w, h)
    
        for y, row in enumerate(self._tileMap()):
            for x, tile in enumerate(row):
                display.blit(tile, (x*TILESIZE, y*TILESIZE))
        display.blit(self.title, 
            ((display.get_width()-self.title.get_width())/2.0,
            (display.get_height()/4-self.title.get_width()/2)))
            
