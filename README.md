Insidiis (Tileset Based Puzzle Platformer)
==========================================

Insidiis is an in-development puzzle-platformer written in Python 3. 
It began life as some simple tile rendering code, and has evolved from there. 
As it stands, it's more of a proof of concept for a game engine than a functioning game, but it's early days yet.

Installing
----------
This is a Python 3 codebase, It's easiest to get running on a virtualenv.
The following instructions have been tested on Linux, but there's no fundamental reason that they shouldn't work on Windows too.

* Download the code by cloning the repository using Git

```
git clone https://bitbucket.org/joe_warren/tileset.git
cd tileset
```

* Set up a virtualenv with python 3

```
virtualenv -p python3 env
```

* Enter that virtualenv
   
```
source env/bin/activate
```

* Install the dependencies

```
pip install -r requirements.txt
```

* Run the game

```
./main.py
```
