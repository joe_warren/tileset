#!/usr/bin/env python3
import pygame
from level import Level
from toggle import Toggle
from entities import Ladder, Boost, Console, Door, Lamp, Sign, Exit

class Level0(Level):
    def __init__(self):
        self.playerTileset = "runner"
        self.background = [
            "t--d__t---d__t---d______",
            "________________________",
            "________________________",
            "________________________",
            "___l_l____l_l____l_l____",
            "__-d_t-__-d_t-__-d_t-___",
            "__$l_£!__$l_%!__$l_%!___",
        ]
        self.grid = [
             "111111111111111111111111",
             "100000000000000000000001",
             "111110111111011111101111",
             "111110111111011111101111",
             "111110111111011111101111",
             "111000001100000110000011",
             "111000001100000110000011",
             "111000001100000110000011",
             "111111111111111111111111"
        ]
        self.tileset = "tilesets/blue_brick"
        self.backgroundTileset = "backgrounds/panel"
        self.playerStart = (32*6.5, 32*7)

        door1 = Door((32*5, 32*4+16), (32, 10), "objects/trapdoor")
        door2 = Door((32*5, 32*2-10), (32, 10), "objects/trapdoor")

        exitlamp = Lamp((32*21, 32), "objects/exit") 

        self.objects = [
            Ladder(5*32, 16, 16+32*8, "objects/ladder.png"),
            Ladder(12*32, 16, 16+32*8, "objects/ladder.png"),
            Ladder(19*32, 16, 16+32*8, "objects/ladder.png"),
            exitlamp,
            door1, door2,
            Door((32*12, 32*4+16), (32, 10), "objects/trapdoor"),
            Door((32*12, 32*2-10), (32, 10), "objects/trapdoor"),
            Door((32*19, 32*4+16), (32, 10), "objects/trapdoor"), 
            Door((32*19, 32*2-10), (32, 10), "objects/trapdoor"), 
            Exit((32*22, 32-1), "objects/portal"), 
            Sign((32*6, 32-1), "objects/sign", 
                "avatars/sign.png", "Cell 45, Cellblock D9"), 
            Sign((32*13, 32-1), "objects/sign", 
                "avatars/sign.png", "Cell 46, Cellblock D9"), 
            Sign((32*20, 32-1), "objects/sign", 
                "avatars/sign.png", "Cell 47, Cellblock D9")
        ]
        super().__init__()

        self.schedule = [
            (4000, lambda s: s.addMessage("avatars/mystery.png", 
                "Hey, lets see about breaking you out of there")), 
            (6000, door1.open),
            (8000, door2.open), 
            (9000, lambda s: s.addMessage("avatars/mystery.png", 
                "That should do it")),
            (10000, lambda s: exitlamp.on(s))
        ]

if __name__ == "__main__":
        from window import Window
        pygame.init()
        window = Window(600, 400)
        Level0().run(window)
