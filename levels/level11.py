#!/usr/bin/env python3
import pygame
import pymunk
from level import Level
from entities import Console, Lamp, Exit, Boost, BoostRobot, Patroller, ClawCrane, Ladder

class Level11(Level):
    def __init__(self):
        self.playerTileset = "runner"
        self.background = [
            "_______l___x__________",
            "_______________________",
            "_______________________",
            "_______________________",
            "__________x-___________",
            "_____-d___l____________",
            "__>___l___l____________",
            "______l___l____________",
            "______l___l____________",
        ]
        self.grid = [
             "11111111111111111111",
             "10000000000000000001",
             "10011111111111111111",
             "10000000000000000001",
             "11000000000000000001",
             "10000100000000000001",
             "10000100000000000001",
             "10000100000000000001",
             "11111100000000000001",
             "11111100000000000001",
             "11111111111111111111"
        ]
        self.tileset = "tilesets/slate"
        self.backgroundTileset = "backgrounds/rock"
        self.playerStart = (32*3, 32*7)

        boostRobot = BoostRobot((32*7, 32*9), (32, 32), "objects/arrow_legs", 
            (0, -1024), "objects/boost/up_unlabeled")

        clawCrane = ClawCrane((32*3+16,32*3-12), 32*3+16, 32*18,
            "objects/claw_crane_top", 
            5, "objects/grabber" )

        craneConsole = Console((32*1+16, 32*7), "objects/console") 
        craneConsole.controls = {
            pygame.K_a: (lambda s: clawCrane.move(-1), 
                         lambda s: clawCrane.cancel_move(), "left" ),
            pygame.K_s: (lambda s: clawCrane.move(1), 
                         lambda s: clawCrane.cancel_move(), "right" ),
            pygame.K_q: (lambda s: clawCrane.grab(), 
                         lambda s: None, "grab")
        }

        exitlamp = Lamp((32*17, 32*1), "objects/exit") 
        exitlamp.on

        self.objects = [
            exitlamp,
            Ladder(32*6 , 16+32*2, 16+32*9, "objects/ladder.png"),
            Exit((32*18, 32*1-1), "objects/portal"),
            Boost((32*1+2, 32*3-2), (0, -1024), "objects/boost/up/"),
            Boost((32*1+2, 32*1), (1024, 0), "objects/boost/right/"),
            craneConsole,
            boostRobot,
            clawCrane.rope,
            clawCrane,
            Patroller(boostRobot, 20),
        ]
        super().__init__()
        self.schedule = [
            (1000, exitlamp.on), 
            (1100, exitlamp.off),
            (1300, exitlamp.on), 
            (1450, exitlamp.off),
            (1600, exitlamp.on), 
            (1650, exitlamp.off),
            (2000, exitlamp.on), 
        ]

if __name__ == "__main__":
        from window import Window
        pygame.init()
        window = Window(600, 400)
        Level11().run(window)
