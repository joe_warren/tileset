#!/usr/bin/env python3
import pygame
from level import Level
from entities import Ladder, Console, Lamp, Exit, Robot, GrapplingDrone

class Level6(Level):
    def __init__(self):
        self.playerTileset = "runner"
        self.background = [
            "-d____l-x_t____",
            "_l____l_l_l____",
            "______x_x_l____",
            "______l_l_t-___",
            "_____tw__-d____",
            "_____t____l____",
            "t____l_>__l____",
            "l____l_________"
        ]
        self.grid = [
             "1111111111111111",
             "1000001000000001",
             "1000001000000001",
             "1110111001111111",
             "1010100001000001",
             "1000000001000001",
             "1000000001000111",
             "1000000000000111",
             "1000000011111111",
             "1111111111111111"
        ]
        self.tileset = "tilesets/slate"
        self.backgroundTileset = "backgrounds/rock"
        self.playerStart = (32*8, 32*7)

        robotConsole = Console((32*1+16, 32*2-1), "objects/console") 
        robot = Robot((32*3, 32*8), (28, 28), "objects/legs")
        robot.console = robotConsole
        robotConsole.controls = {
            pygame.K_a: (lambda s: robot.move(-20), 
                         lambda s: robot.cancel_move(), "left" ),
            pygame.K_s: (lambda s: robot.move(20), 
                         lambda s: robot.cancel_move(), "right" )
        }

        droneConsole = Console((32*4+16, 32*2-1), "objects/console") 
        drone = GrapplingDrone((32*2, 32*5), (30, 16), "objects/drone", 
            5, "objects/grabber" )
        exitlamp = Lamp((32*13, 32*5), "objects/exit") 
        droneConsole.controls = {
            pygame.K_a: (lambda s: drone.move((-120, 0)), 
                         lambda s: drone.cancel_move(), "left" ),
            pygame.K_w: (lambda s: drone.move((0, -120)), 
                         lambda s: drone.cancel_move(), "up" ),
            pygame.K_d: (lambda s: drone.move((120, 0)), 
                         lambda s: drone.cancel_move(), "right" ),
            pygame.K_s: (lambda s: drone.move((0, 120)), 
                         lambda s: drone.cancel_move(), "down" ),
            pygame.K_q: (lambda s: drone.toggle(), 
                         lambda s: s, "deploy" )
        }
        self.objects = [
            Ladder(32*3 , 16, 16+32*7, "objects/ladder.png"),
            droneConsole,
            robot, 
            robotConsole,
            exitlamp,
            Exit((32*14, 32*5-1), "objects/portal"),
            drone.rope,
            drone
            ]
        super().__init__()
        self.schedule = [
            (1000, exitlamp.on), 
            (1100, exitlamp.off),
            (1300, exitlamp.on), 
            (1450, exitlamp.off),
            (1600, exitlamp.on), 
            (1650, exitlamp.off),
            (2000, exitlamp.on), 
        ]

if __name__ == "__main__":
        from window import Window
        pygame.init()
        window = Window(600, 400)
        Level6().run(window)
