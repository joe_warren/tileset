#!/usr/bin/env python3
import pygame
import pymunk
from level import Level
from entities import Ladder, Console, Lamp, Exit, Patroller, FlightController, Drone


class Level10(Level):
    def __init__(self):
        self.playerTileset = "runner"
        self.background = [
            "-xx______________________",
            ">l___o__o_______o__o_____",
            "_________________________",
            "_________________________",
            "__________x-_____________",
            "_____d____l______________",
            "_____l____l______________",
        ]
        self.grid = [
             "11111111111111111111111111",
             "10000000000000000000000001",
             "10000000000000000000000001",
             "11100000000011100000000011",
             "11100000000001000000000011",
             "11000000000000000000000011",
             "10000000000000000000000011",
             "10000000000000000000000011",
             "11111111111111111111111111"
        ]
        self.tileset = "tilesets/castle"
        self.backgroundTileset = "backgrounds/castle"
        self.playerStart = (32*1, 32*2)
        exitlamp = Lamp((32*23, 32*2), "objects/exit") 
        exitlamp.on
        platform1 = Drone((32*5, 32*3),(30,14), "objects/hover_platform", mass=15.0)
        platform2 = Drone((32*17, 32*3),(30,14), "objects/hover_platform", mass=15.0)
        self.objects = [
            exitlamp,
            Exit((32*24, 32*2-1), "objects/portal"),
            Ladder(32*3 , 16+16, 16+32*7, "objects/ladder.png"),
            platform1,
            Patroller(platform1, pymunk.Vec2d(500, 0)),
            FlightController(platform1, (0, 32*3), (0,125)),
            platform2,
            Patroller(platform2, pymunk.Vec2d(500, 0)),
            FlightController(platform2, (0, 32*3), (0,125))

        ]
        super().__init__()
        self.schedule = [
            (1000, exitlamp.on), 
            (1100, exitlamp.off),
            (1300, exitlamp.on), 
            (1450, exitlamp.off),
            (1600, exitlamp.on), 
            (1650, exitlamp.off),
            (2000, exitlamp.on), 
        ]

if __name__ == "__main__":
        from window import Window
        pygame.init()
        window = Window(600, 400)
        Level10().run(window)
