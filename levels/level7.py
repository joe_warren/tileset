#!/usr/bin/env python3
import pygame
from level import Level
from entities import Ladder, Console, Lamp, Exit, BoostRobot, Boost, Patroller

class Level7(Level):
    def __init__(self):
        self.playerTileset = "runner"
        self.background = [
            "-d________l__",
            "_l________l__",
            "_________-x--",
            "__________l__",
            "__________l__",
            "_____d____l__",
            "_____l_>__l__",
        ]
        self.grid = [
             "11111111111111",
             "10000000000001",
             "10000000000001",
             "11111111110001",
             "10000000000001",
             "10000000000001",
             "10000000000001",
             "10000000000001",
             "11111111111111"
        ]
        self.tileset = "tilesets/slate"
        self.backgroundTileset = "backgrounds/rock"
        self.playerStart = (32*8, 32*7)

        robot = BoostRobot((32*2, 32*7), (32, 32), "objects/arrow_legs", 
            (0, -1024), "objects/boost/up_unlabeled")
        exitlamp = Lamp((32*3, 32*2), "objects/exit") 
        exitlamp.on
        self.objects = [
            robot,
            Patroller(robot, 20),
            Boost((32*12, 32*1), (-1024, 0), "objects/boost/left"),
            exitlamp,
            Exit((32*4, 32*2-1), "objects/portal")
        ]
        super().__init__()
        self.schedule = [
            (1000, exitlamp.on), 
            (1100, exitlamp.off),
            (1300, exitlamp.on), 
            (1450, exitlamp.off),
            (1600, exitlamp.on), 
            (1650, exitlamp.off),
            (2000, exitlamp.on), 
        ]

if __name__ == "__main__":
        from window import Window
        pygame.init()
        window = Window(600, 400)
        Level7().run(window)
