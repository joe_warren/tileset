#!/usr/bin/env python3
import pygame
from level import Level
from entities import Ladder, Console, Lamp, Exit, BoostRobot, Boost, Patroller, Console, Robot

class Level9(Level):
    def __init__(self):
        self.playerTileset = "runner"
        self.background = [
            "d_________l__",
            "l_________l__",
            "_________-x--",
            "__________l__",
            "__________l__",
            "_____d____l__",
            "_____l_>__l__",
        ]
        self.grid = [
             "11111111111111",
             "10000100000001",
             "10000100000001",
             "11011111110001",
             "10000000000001",
             "10000000000001",
             "10000000000001",
             "10000000000001",
             "11111111111111"
        ]
        self.tileset = "tilesets/castle"
        self.backgroundTileset = "backgrounds/castle"
        self.playerStart = (32*8, 32*7)

        boostRobot = BoostRobot((32*2, 32*7), (32, 32), "objects/arrow_legs", 
            (0, -1024), "objects/boost/up_unlabeled")


        robotConsole = Console((32*3+16, 32*2-1), "objects/console") 
        robot = Robot((32*5, 32*7), (28, 28), "objects/legs")
        robot.console = robotConsole
        robotConsole.controls = {
            pygame.K_a: (lambda s: robot.move(-20), 
                         lambda s: robot.cancel_move(), "left" ),
            pygame.K_s: (lambda s: robot.move(20), 
                         lambda s: robot.cancel_move(), "right" )
        }

        exitlamp = Lamp((32*7, 32*2), "objects/exit") 
        exitlamp.on
        self.objects = [
            boostRobot,
            Patroller(boostRobot, 20),
            robot, 
            robotConsole,
            Boost((32*12, 32*1), (-1024, 0), "objects/boost/left"),
            exitlamp,
            Exit((32*8, 32*2-1), "objects/portal")
        ]
        super().__init__()
        self.schedule = [
            (1000, exitlamp.on), 
            (1100, exitlamp.off),
            (1300, exitlamp.on), 
            (1450, exitlamp.off),
            (1600, exitlamp.on), 
            (1650, exitlamp.off),
            (2000, exitlamp.on), 
        ]

if __name__ == "__main__":
        from window import Window
        pygame.init()
        window = Window(600, 400)
        Level9().run(window)
