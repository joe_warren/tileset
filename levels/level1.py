#!/usr/bin/env python3
import pygame
from level import Level
from toggle import Toggle
from entities import Ladder, Boost, Console, Door,Lamp, Exit

class Level1(Level):
    def __init__(self):
        self.playerTileset = "runner"
        self.background = [
            "_____l___l_____",
            "_____l___t-----",
            "_____tm--w-x---",
            "_____ll____l___",
            "_ll_t-x----w---",
            "_tw-w-w-_______",
            "_______>_______",
        ]
        self.grid = [
             "1111111111111111",
             "1010000011000001",
             "1010100011000001",
             "1000100000011111",
             "1011111011110001",
             "1000001000000001",
             "1000001001111111",
             "1111100000000001",
             "1111111111111111"
        ]
        self.tileset = "tilesets/blue_brick"
        self.backgroundTileset = "backgrounds/panel"
        self.playerStart = (32*8, 32*7)

        console = Console((32*13, 32*5-1), "objects/console") 
        door = Door((32*9, 32*7), (10, 32), "objects/door")
        lamps = [Lamp((32*(10+i), 32*7-6), "objects/lamp") for i in range(0, 3)]
        exitlamp = Lamp((32*13, 32*7), "objects/exit") 

        def consoleOn(level):
            door.open(level)
            exitlamp.on(level)
            for i, l in enumerate(lamps):
                self.delay((i+1)*750, l.on)

        def consoleOff(level):
            door.close(level)
            exitlamp.off(level)
            for i, l in enumerate(lamps):
                self.delay((i+1)*750, l.off)

        console.controls = {
            pygame.K_a: Toggle((consoleOn, "open door"), (consoleOff, "close door"))
        }
        self.objects = [
            Ladder(32, 16, 16+32*6, "objects/ladder.png"),
            Ladder(32*3, 16, 16+32*3, "objects/ladder.png"),
            Ladder(32*5, 16, 16+32*3, "objects/ladder.png"),
            Boost((32*7-2, 32*7-1), (0, -1024), "objects/boost/up/"),
            Boost((32*7, 32*2), (-1024, 0), "objects/boost/left/"),
            Boost((32*5+2, 32*7), (1024, 0), "objects/boost/right/"),
            console,
            door, 
            exitlamp,
            Exit((32*14, 32*7-1), "objects/portal")
        ] + lamps
        super().__init__()

if __name__ == "__main__":
        from window import Window
        pygame.init()
        window = Window(600, 400)
        Level1().run(window)
