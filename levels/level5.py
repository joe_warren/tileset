#!/usr/bin/env python3
import pygame
from level import Level
from entities import Ladder, Console, Lamp, Exit, GrapplingDrone, Crate

class Level5(Level):
    def __init__(self):
        self.playerTileset = "runner"
        self.background = [
            "-d______t-x----",
            "_l______l_l____",
            "______--x-x----",
            "_____l__l_l____",
            "_____t--w-d____",
            "_____l____l____",
            "_____l_>__l____",
        ]
        self.grid = [
             "1111111111111111",
             "1000001000000001",
             "1000001000000001",
             "1110111000000001",
             "1010100000000001",
             "1000000000001111",
             "1000000000001111",
             "1000000000001111",
             "1111111111111111"
        ]
        self.tileset = "tilesets/blue_brick"
        self.backgroundTileset = "backgrounds/panel"
        self.playerStart = (32*8, 32*7)

        console = Console((32*4+16, 32*2-1), "objects/console") 
        drone = GrapplingDrone((32*2, 32*5), (30, 16), "objects/drone", 
            5, "objects/grabber" )
        exitlamp = Lamp((32*13, 32*4), "objects/exit") 
        console.controls = {
            pygame.K_a: (lambda s: drone.move((-120, 0)), 
                         lambda s: drone.cancel_move(), "left" ),
            pygame.K_w: (lambda s: drone.move((0, -120)), 
                         lambda s: drone.cancel_move(), "up" ),
            pygame.K_d: (lambda s: drone.move((120, 0)), 
                         lambda s: drone.cancel_move(), "right" ),
            pygame.K_s: (lambda s: drone.move((0, 120)), 
                         lambda s: drone.cancel_move(), "down" ),
            pygame.K_q: (lambda s: drone.toggle(), 
                         lambda s: s, "deploy" )
        }
        self.objects = [
            Ladder(32*3 , 16, 16+32*7, "objects/ladder.png"),
            console,
            exitlamp,
            Exit((32*14, 32*4-1), "objects/portal"),
            drone.rope,
            drone,
            Crate((32*12, 32*4.0), (32, 32), 4, 0.8, "objects/crate"),
            Crate((32*12, 32*3.0), (32, 32), 4, 0.8, "objects/crate"),
            Crate((32*12, 32*2.0), (32, 32), 4, 0.8, "objects/crate")
            ]
        super().__init__()
        self.schedule = [
            (1000, exitlamp.on), 
            (1100, exitlamp.off),
            (1300, exitlamp.on), 
            (1450, exitlamp.off),
            (1600, exitlamp.on), 
            (1650, exitlamp.off),
            (2000, exitlamp.on), 
        ]

if __name__ == "__main__":
        from window import Window
        pygame.init()
        window = Window(600, 400)
        Level5().run(window)
