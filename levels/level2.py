#!/usr/bin/env python3
import pygame
from level import Level
from entities import Ladder, Console, Lamp, Exit, Robot

class Level2(Level):
    def __init__(self):
        self.playerTileset = "runner"
        self.background = [
            "-d______t-x----",
            "_l______l_l____",
            "______--x-x----",
            "_____l__l_l____",
            "_____t--w-d____",
            "_____l____l____",
            "_____l_>__l____",
        ]
        self.grid = [
             "1111111111111111",
             "1000001000000001",
             "1000001000000001",
             "1110111000000001",
             "1010100000000001",
             "1000000000000001",
             "1000000000001111",
             "1000000000001111",
             "1111111111111111"
        ]
        self.tileset = "tilesets/blue_brick"
        self.backgroundTileset = "backgrounds/panel"
        self.playerStart = (32*8, 32*7)

        console = Console((32*4+16, 32*2-1), "objects/console") 
        robot = Robot((32*2, 32*7), (29, 29), "objects/legs")
        robot.console = console
        exitlamp = Lamp((32*13, 32*5), "objects/exit") 
        exitlamp.on
        console.controls = {
            pygame.K_a: (lambda s: robot.move(-20), 
                         lambda s: robot.cancel_move(), "left" ),
            pygame.K_s: (lambda s: robot.move(20), 
                         lambda s: robot.cancel_move(), "right" )
        }
        self.objects = [
            Ladder(32*3 , 16, 16+32*7, "objects/ladder.png"),
            console,
            robot,
            exitlamp,
            Exit((32*14, 32*5-1), "objects/portal")
        ]
        super().__init__()
        self.schedule = [
            (1000, exitlamp.on), 
            (1100, exitlamp.off),
            (1300, exitlamp.on), 
            (1450, exitlamp.off),
            (1600, exitlamp.on), 
            (1650, exitlamp.off),
            (2000, exitlamp.on), 
        ]

if __name__ == "__main__":
        from window import Window
        pygame.init()
        window = Window(600, 400)
        Level2().run(window)
