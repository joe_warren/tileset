#!/usr/bin/env python3
import pygame
from level import Level
from entities import Ladder, Console, Lamp, Exit, Robot, Boost, Patroller

class Level8(Level):
    def __init__(self):
        self.playerTileset = "red_runner"
        self.background = [
            "[][][][][][][",
            "][E[][][][][]",
            "[][][][][][][",
            "][][][][][][]",
            "[][][][][][][",
            "][][][][][][]",
            "[][][]>][][]["
        ]
        self.grid = [
             "11111111111111",
             "10000000000001",
             "10000000000001",
             "11111111100001",
             "10000000000011",
             "10000000000111",
             "10000000001111",
             "10000000001111",
             "11111111111111"
        ]
        self.tileset = "tilesets/red_brick"
        self.backgroundTileset = "backgrounds/white_brick"
        self.playerStart = (32*8, 32*7)

        robot = Robot((32*2, 32*7), (28, 28), "objects/legs")
        self.objects = [
            robot,
            Patroller(robot, 20),
            Ladder(32*11 , 16, 16+32*4, "objects/ladder.png"),
            Boost((32*12, 32*1), (-1024, 0), "objects/boost/left"),
            Exit((32*4, 32*2-1), "objects/hole")
        ]
        super().__init__()

if __name__ == "__main__":
        from window import Window
        pygame.init()
        window = Window(600, 400)
        Level8().run(window)
