import pygame
import glob
from os import path

def LoadAnimation(directory):
    count = len(glob.glob(path.join(directory , "*.png")))

    def nameOfFile(i):
        return path.join(directory , str(i) + ".png")

    return [pygame.image.load(nameOfFile(i)) for i in range(0, count)]

def LoadAnimationDictionary(directory, categories):
    return {cat: LoadAnimation(path.join(directory, cat)) for cat in categories}

FRAMERATE = 15

def RenderTransition(surface, animation, position, eventTime, backward=False, time=None):
    if time == None:
        time = pygame.time.get_ticks()
    timePassed = time - eventTime
    framesPassed = int(timePassed*FRAMERATE/1000)
    frame = max(0, min(framesPassed, len(animation)-1))
    if backward:
        frame = -frame-1
    img = animation[frame]
    surface.blit(img, 
        (position[0] - img.get_width()/2, position[1] - img.get_height()/2 ))

def Render(surface, img, position):
    surface.blit(img, 
        (position[0] - img.get_width()/2, position[1] - img.get_height()/2 ))

def RenderAnimation(surface, animation, position, time=None):
    if time == None:
        time = pygame.time.get_ticks()
    frame = int(time*FRAMERATE/1000)%len(animation)
    img = animation[frame]
    Render(surface, img, position)

def RenderAnimationDictionary(surface, animationDict, position, state, time=None):
    RenderAnimation(surface, animationDict[state], position, time)

