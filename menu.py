import pygame 
class Menu():
    def __init__(self, title, items):
        self.title = title
        self.items = items
        self.activeIndex = 0
        self.top = 0

        self.controls = {
            pygame.K_UP: (self.up, lambda x: x, ""),
            pygame.K_DOWN: (self.down, lambda x: x, ""),
            pygame.K_RETURN: (self.enter, lambda x: x, "")
        }
    def available(self, level):
        return True
    def scrollUp(self):
        if self.top > 0:
            self.top -= 1
    def scrollDown(self):
        if self.top < len(self.items)-1:
            self.top += 1

    def up(self, level):
        if self.activeIndex > 0:
            self.activeIndex -= 1
        if self.activeIndex < self.top:
            self.top = self.activeIndex
    def down(self, level):
        if self.activeIndex < len(self.items)-1:
            self.activeIndex += 1
    def select(self, index):
        if index < len(self.items) and index >= 0:
            self.activeIndex = index
    def enter(self, level):
        self.items[self.activeIndex][1]()

    def getItems(self):
        for i, item in enumerate(self.items):
            yield (item[0], item[1], i == self.activeIndex)

    
    
