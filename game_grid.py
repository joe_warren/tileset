#!/usr/bin/env python3

from itertools import product
import pygame
import pymunk
TILESIZE = 32

class GameGrid():
    def __init__(self, grid, tiledir):
        self.tiledir = tiledir

        self.grid = grid
        self.height = len(self.grid)
        self.width = len(self.grid[0])

        tiles = [t for t in product(["0", "1"], repeat=4) 
            if t !=("0", "0", "0", "0")]
        self.tilesImgs = {t: pygame.image.load(self._nameOfTileFile(t)) 
             for t in tiles}
        self.tileMap = self._gridToTileMap(self.grid)

    def render(self, display):
        for x in range(0,self.width-1):
            for y in range(0,self.height-1):
                tile = self.tileMap[y][x]
                if tile != ("0","0","0","0"):
                    display.blit(self.tilesImgs[tile], (x*TILESIZE, y*TILESIZE))

    def addToPhysics(self, space):
        boxes = []
        def contained(x_, y_):
            return any(((x_, y_) in a for a in boxes))

        for y_, line  in enumerate(self.grid):
            for x_, point in enumerate(line):
                if point == "1" and not contained(x_, y_) :
                    v = set()
                    v.add((x_, y_))

                    x2 = x_+1
                    while x2 < self.width:
                        p2 = self.grid[y_][x2]
                        if p2 == "1" and not contained(x2, y_) :
                            v.add((x2, y_))
                            x2 += 1
                        else:
                            break
                    y2 = y_ + 1
                    while y2 < self.height:
                        p3 = {(w[0], w[1]+1) for w in v}
                        full = [self.grid[p[1]][p[0]]=='1' for p in p3]
                        notreached = [not contained(p[0], p[1]) for p in p3]

                        if all(full) and all(notreached):
                            v |= p3
                        y2 += 1
                    boxes.append(v)

        for box in boxes:
            minx = min((b[0] for b in box))
            miny = min((b[1] for b in box))
            maxx = max((b[0] for b in box))+1
            maxy = max((b[1] for b in box))+1
            midx = (minx + maxx)/2.0
            midy = (miny + maxy)/2.0
            width = maxx - minx
            height = maxy - miny 
            body = pymunk.Body(0, 0, pymunk.Body.STATIC)
            body.position = (midx*32-16, midy*32-16)
            poly = pymunk.Poly.create_box(body, size=(width*32,height*32))
            # setting the walls to be perfectly elastic allows all of the 
            # objects to specify a per object elasticity
            # as elasticity for a collision is calculated by multiplying 
            #the elasticity of the underlying shapes

            poly.elasticity = 1.0
            poly.friction = 1
            space.add(body, poly)

    def _nameOfTileFile(self, tile):
        return self.tiledir + "/" + ("".join(tile)) + ".png"

    def _gridToTileMap(self, grid):
        height = len(grid)
        width = len(grid[0])
        def lookup(x,y):
            return (grid[y][x], grid[y][x+1], grid[y+1][x], grid[y+1][x+1])
        return [[lookup(x, y) for x in range(0, width-1)] for y in range(0,height-1)]

