import pygame
from collections import namedtuple

KEY_OUTLINE_COLOUR = (0,0,0)
KEY_HIGHLIGHT_COLOUR = (150,200,255)
KEY_SHADOW_COLOUR = (50,75,150)
KEY_COLOUR = (100, 150, 255)
KEY_TEXT_COLOUR = (0, 0, 0)
ACTION_TEXT_COLOUR = (100, 150, 255)
ACTION_TEXT_OUTLINE_COLOUR = (0, 0, 0)

ACTIVE_KEY_HIGHLIGHT_COLOUR = (50,75,255)
ACTIVE_KEY_SHADOW_COLOUR = (150,200,255)
ACTIVE_KEY_COLOUR = (100, 150, 255)
ACTIVE_ACTION_TEXT_COLOUR = (150, 200, 255)

TEXT_SIZE = 10

MENU_BACKGROUND_COLOUR = (0,0,0)
MENU_FOREGROUND_COLOUR = (100, 150, 255)
MENU_ACTIVE_COLOUR = (200, 225, 255)

MESSAGE_TEXT_COLOUR = (100, 150, 255)
MESSAGE_TEXT_OUTLINE_COLOUR = (0, 0, 0)

BBox =  namedtuple("BBox", ["tl", "br"])
ClickableBBox = namedtuple("ClickableBBox", ["bb", "onhover", "onclick", "key"])

class HUD():
    def __init__(self):
        self.font = pygame.font.Font("OCRA.ttf", TEXT_SIZE)
        self.layercache={}

    def render_single_control(self, key, action, active):
        highlight = KEY_HIGHLIGHT_COLOUR
        shadow = KEY_SHADOW_COLOUR
        main = KEY_COLOUR
        text = ACTION_TEXT_COLOUR
        if active:
            highlight = ACTIVE_KEY_HIGHLIGHT_COLOUR
            shadow = ACTIVE_KEY_SHADOW_COLOUR
            main = ACTIVE_KEY_COLOUR
            text = ACTIVE_ACTION_TEXT_COLOUR

        k = self.font.render(pygame.key.name(key), True, KEY_TEXT_COLOUR)
        a = self.font.render(action, True, text)
        o = self.font.render(action, True, ACTION_TEXT_OUTLINE_COLOUR)
        kw, kh = k.get_size()
        aw, ah = a.get_size() 

        mh = max(kh, ah)
        surface = pygame.Surface(( kw + aw + 20, 9 + mh ), pygame.SRCALPHA, 32)
        surface = surface.convert_alpha()

        # inefficiently draw a button
        # this should be fine, as we cache the result
        # so  I won't worry too much about it
        surface.fill(KEY_OUTLINE_COLOUR, pygame.Rect((0,1), (kw+8, mh+6)))
        surface.fill(KEY_OUTLINE_COLOUR, pygame.Rect((1,0), (kw+6, mh+8)))
        surface.fill(main, pygame.Rect((2,2), (kw+4, mh+4)))
        surface.fill(highlight, pygame.Rect((1,2), (1, mh+4)))
        surface.fill(highlight, pygame.Rect((2,1), (kw+4, 1)))
        surface.fill(shadow, pygame.Rect((kw+6,2), (1, mh+4)))
        surface.fill(shadow, pygame.Rect((2,mh+6), (kw+4, 1)))
        if active:
            surface.blit(k, (5,5))
        else:
            surface.blit(k, (4,4))
        # inefficiently fake outlined text
        for i in range(-1,2):
            for j in range(-1,2):
                surface.blit(o, (kw+15+i,4+j))
        surface.blit(a, (kw+15,4))

        return surface

    def render_controls(self, window, objects):
        yOff = 1
        pressed = pygame.key.get_pressed()
        for o in objects:
            for key, action in o.controls.items():
                active = pressed[key]
                if len(action) > 2:
                    name = (key, action[2], active)
                    if not name in self.layercache:
                        self.layercache[name] = self.render_single_control(*name)
                    window.display.blit(self.layercache[name], (1, yOff))
                    yOff += self.layercache[name].get_height()
    def render_menu(self, window, menu):
        def getText(texCol):
            if not texCol in self.layercache:
                self.layercache[texCol] = self.font.render(texCol[0], True, texCol[1])
            return self.layercache[texCol]
        
        titleImg = getText((menu.title, MENU_FOREGROUND_COLOUR))
        def itemColour(i):
            if i[2]:
                return MENU_ACTIVE_COLOUR
            return MENU_FOREGROUND_COLOUR

        imgs = [getText((i[0], itemColour(i))) for i in menu.getItems()]

        maxItems = int(((window.display.get_height()*3)/4)/30)
        if maxItems > len(imgs):
            maxItems = len(imgs)
        menu.top = min(menu.top, max(0, len(imgs)-maxItems))
        if menu.top > menu.activeIndex:
            menu.activeIndex = menu.top
        if menu.top + maxItems <= menu.activeIndex:
            menu.top = menu.activeIndex +1 - maxItems 
        imgs = imgs[menu.top:menu.top+maxItems]

        width = 20 + max([i.get_width() for i in imgs + [titleImg]])
        height = 20 + sum([i.get_height()+10 for i in imgs + [titleImg]])
        sx = (window.display.get_width()- width)/2
        sy = (window.display.get_height()-height)/2
        titleHeight = titleImg.get_height()+10 

        window.display.fill(MENU_BACKGROUND_COLOUR, pygame.Rect((sx,sy), (width, height)))
        window.display.fill(MENU_FOREGROUND_COLOUR, pygame.Rect((sx+1,sy+1), (width-2, height-2)))
        window.display.fill(MENU_BACKGROUND_COLOUR, pygame.Rect((sx+2,sy+2), (width-4, titleHeight)))
        window.display.fill(MENU_BACKGROUND_COLOUR, pygame.Rect((sx+2,sy+3 + titleHeight), (width-4, height - titleHeight - 5  )))

        midX = window.display.get_width()/2
        window.display.blit(titleImg, (midX - titleImg.get_width()/2.0, sy+5+3))

        yOff = sy + titleHeight + 15
        for img in imgs:
            window.display.blit(img, (midX - img.get_width()/2.0, yOff))
            yOff += img.get_height() + 10

    def menu_bounding_boxes(self, window, menu):
        def getText(texCol):
            if not texCol in self.layercache:
                self.layercache[texCol] = self.font.render(texCol[0], True, texCol[1])
            return self.layercache[texCol]
        
        titleImg = getText((menu.title, MENU_FOREGROUND_COLOUR))
        imgs = [getText((i[0], MENU_FOREGROUND_COLOUR)) for i in menu.getItems() ]

        maxItems = int(((window.display.get_height()*3)/4)/30)
        if maxItems > len(imgs):
            maxItems = len(imgs)
        menu.top = min(menu.top, max(0, len(imgs)-maxItems))
        if menu.top > menu.activeIndex:
            menu.activeIndex = menu.top
        if menu.top + maxItems <= menu.activeIndex:
            menu.top = menu.activeIndex +1 - maxItems 
        imgs = imgs[menu.top:menu.top+maxItems]

        width = 20 + max([i.get_width() for i in imgs + [titleImg]])
        height = 20 + sum([i.get_height()+10 for i in imgs + [titleImg]])
        sx = (window.display.get_width()- width)/2
        sy = (window.display.get_height()-height)/2
        titleHeight = titleImg.get_height()+10 
        midX = window.display.get_width()/2
        window.display.blit(titleImg, (midX - titleImg.get_width()/2.0, sy+5+3))

        yOff = sy + titleHeight + 15
        boundingBoxes = []
        def hover(i):
            return lambda x: menu.select(i)

        def click(item):
            return lambda x: item[1]()

        items = list(menu.getItems())[menu.top:menu.top+maxItems]
        
        for i, (img, item) in enumerate(zip(imgs, items)):
            bb = BBox((midX - img.get_width()/2.0, yOff), (midX + img.get_width()/2.0, yOff + img.get_height()))
            boundingBoxes.append(ClickableBBox(bb, hover(menu.top+i), click(item), 1))
            yOff += img.get_height() + 10

        screenBBox = BBox((sx, sy),(sx+width, sy+height))
        boundingBoxes.append(ClickableBBox(screenBBox, lambda x: x, lambda x: menu.scrollUp(), 4))
        boundingBoxes.append(ClickableBBox(screenBBox, lambda x: x, lambda x: menu.scrollDown(), 5)) 
        return boundingBoxes

    def wrapped_text(self, text, width):
        ta = text.split(" ")
        line = ""
        lines = []
        while len(ta) > 0: 
            nextline = ta[0]
            if self.font.size(nextline)[0] > width:
                line = nextline
                ta.pop(0)
            else:
                while len(ta) > 0 and self.font.size(nextline)[0] < width:
                    line = nextline
                    ta.pop(0)
                    n = ""
                    if len(ta) > 0:
                        n = ta[0]
                    nextline = line + " " +  n
            lines.append(line)
        return lines

    def wrapped_render(self, surface, pos, text):
        for i, line in enumerate(text):
            s = self.font.render(line, True, MESSAGE_TEXT_COLOUR)
            o = self.font.render(line, True, MESSAGE_TEXT_OUTLINE_COLOUR)
            x = pos[0]
            y = pos[1] + i * self.font.get_linesize()
            # inefficiently fake outlined text
            for j in range(-1,2):
                for k in range(-1,2):
                    surface.blit(o, (x+j,y+k))
            surface.blit(s, (x, y))

    def render_message(self, window, filename, message, yoff):
        if not filename in self.layercache:
            self.layercache[filename] = pygame.image.load(filename)
        image = self.layercache[filename]
        width = window.display.get_width()
        if not (filename, message, width) in self.layercache:
            wrapped_text = self.wrapped_text(message, width-image.get_width())
            textheight = self.font.get_linesize() * len(wrapped_text) +5
            height = max(image.get_height(), textheight)
            surface = pygame.Surface(( width, height ), pygame.SRCALPHA, 32)
            surface.blit(image, (0,0))
            print(wrapped_text)
            self.wrapped_render(surface, (image.get_width(), 5), wrapped_text)
            self.layercache[(filename, message, width)] = surface
        surf = self.layercache[(filename, message, width)] 
        window.display.blit( surf, (0, window.display.get_height() - surf.get_height()-yoff))
        return surf.get_height()
    def render_messages(self, window, messages):
        yoff = 0
        for m in messages:
            yoff += self.render_message(window, m[0], m[1], yoff)
        
    def render_page_text(self, window, text):
        width = 0
        height = 0
        for t in text + ["continue"]:
            sz = self.font.size(t)
            height += sz[1] + 10
            width = max(width, sz[0])

        height += 10
        width += 20
        sx = (window.display.get_width() - width)/2.0
        sy = (window.display.get_height() - height)/2.0
        window.display.fill(MENU_FOREGROUND_COLOUR, pygame.Rect((sx,sy), (width, height)))
        window.display.fill(MENU_BACKGROUND_COLOUR, pygame.Rect((sx+1,sy+1), (width-2, height-2)))
        yoff = sy + 10
        for t in text:
            surf = self.font.render(t, True, MENU_FOREGROUND_COLOUR)
            sx = (window.display.get_width() - surf.get_width())/2.0
            window.display.blit( surf, (sx, yoff))
            yoff += surf.get_height() + 10

        surf = self.font.render("continue", True, MENU_ACTIVE_COLOUR)
        sx = (window.display.get_width() - surf.get_width())/2.0
        window.display.blit( surf, (sx, yoff))

