import pygame
from os import path
from game_grid import TILESIZE

class Background():
    def __init__(self, grid, tiledir):
        self.tiledir = tiledir

        self.grid = grid
        self.height = len(self.grid)
        self.width = len(self.grid[0])

        tilenames = set("".join(grid))

        self.tilesImgs = {t: pygame.image.load(self._nameOfTileFile(t)) 
             for t in tilenames}

        self.tilesize = next (iter (self.tilesImgs.values())).get_size()

    def _nameOfTileFile(self, tile):
        return path.join(self.tiledir, tile + ".png")

    def render(self, display):
        for y, row in enumerate(self.grid):
            for x, t  in enumerate(row):
                tile = self.tilesImgs[t]
                display.blit(tile, (x*self.tilesize[0]+TILESIZE/2, y*self.tilesize[1]+TILESIZE/2))
