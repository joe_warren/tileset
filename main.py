#!/usr/bin/env python3
import pygame
import pymunk
import pymunk.pygame_util
from hud import HUD
from menu import Menu
from menu_background import MenuBackground
from window import Window
from levels import *
from level import Result
from input import Input

class Continue(Exception):
    pass

class Main():
    def __init__(self):
        self.menu = Menu( "Insidiis", [
            ("New Game", self.newGame),
            ("Select Level", self.level_select),
            ("Credits", self.display_credits), 
            ("Quit", pygame.quit)
            ])
        self.background = MenuBackground("title.png", "backgrounds/panel")
        self.interuptingMenu = Menu( "none", [
            ("continue", self.interupt )
            ])

        pygame.init()
        self.window = Window(576, 416)
        self.levels = [
            ("Level 0", Level0), 
            ("Level 1", Level1),
            ("Level 2", Level2),
            ("Level 3", Level3),
            ("Level 4", Level4),
            ("Level 5", Level5),
            ("Level 6", Level6),
            ("Level 7", Level7),
            ("Level 8", Level8),
            ("Level 9", Level9),
            ("Level 10", Level10),
            ("Level 11", Level11),
            ("Back", self.interupt)
        ]
        self.levelMenu =  Menu( "Level Select", [
                (self.levels[i][0], self._runLevelFn(i) )
                    for i in range(0, len(self.levels))
            ]
        )
    def _runLevelFn(self, i):
        def run():
            self.newGame(self.levels[i:])
        return run
    def interupt(self):
        raise Continue()

    def display_text(self, text):
        hud = HUD()
        clock = pygame.time.Clock()
        input = Input(self)
        try:
            while True:
                input.get(self.window, [self.interuptingMenu])
                self.window.display.fill((0,0,0))
                self.background.render(self.window.display)
                hud.render_page_text(self.window, text)
                pygame.display.flip()
                clock.tick(30) 
        except Continue:
            pass

    def display_time(self, level, time):
         self.display_text([
             level, "completed in: {:.2f}s".format(time/1000)])

    def display_credits(self):
         self.display_text([
            "Insidiis", 
            "By (Hungry) Joe Warren", 
            "Made in Python", 
            "with PyGame & PyMunk" ])

    def newGame(self, levels=None):
        if levels is None:
            levels = self.levels
        for l in levels:
           res, time = l[1]().run(self.window)
           if res == Result.EXIT:
               return
           self.display_time(l[0], time)

    def level_select(self):
        clock = pygame.time.Clock()
        input = Input(self)
        hud = HUD()
        try:
            while True:
                input.get(self.window, [self.levelMenu], hud.menu_bounding_boxes(self.window, self.levelMenu))
                self.window.display.fill((0,0,0))
                self.background.render(self.window.display)
                hud.render_menu(self.window, self.levelMenu)
                pygame.display.flip()
                clock.tick(30) 
        except Continue:
            pass

    def run(self):
        clock = pygame.time.Clock()
        input = Input(self)
        hud = HUD()

        while True:
            try:
                input.get(self.window, [self.menu], hud.menu_bounding_boxes(self.window, self.menu))
                self.window.display.fill((0,0,0))
                self.background.render(self.window.display)
                hud.render_menu(self.window, self.menu)
                pygame.display.flip()
                clock.tick(30) 
            except Continue:
                pass

if __name__ == "__main__":
    Main().run()

