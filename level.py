import pygame
import pymunk
#import pymunk.pygame_util

from player import Player
from hud import HUD
from menu import Menu
from game_grid import GameGrid, TILESIZE
from background import Background
from input import Input
from enum import Enum

class Result(Enum):
    EXIT = "EXIT"
    SUCCESS = "SUCCESS"

class Level():
    def __init__(self):
        self.paused = False
        self.result = None

        self.input = Input(self)

        self.space = pymunk.Space()
        self.space.gravity = (0,1024)

        self.player = Player(self.playerStart, self.playerTileset)
        self.player.addToPhysics(self.space)

        self.grid = GameGrid(self.grid, self.tileset)
        self.background = Background(self.background, self.backgroundTileset)
        self.messages = []

        self.timer = 0
        self.schedule = [] 

        self.surface = pygame.Surface((
                (self.grid.width-1)*TILESIZE, 
                (self.grid.height-1)*TILESIZE
                ))
        self.grid.addToPhysics(self.space)

        self.winOffX = 0
        self.winOffY = 0

        for o in self.objects:
            o.addToPhysics(self.space)

    def delay(self, delay, action):
        self.schedule.append((self.timer + delay, action)) 

    def addMessage(self, filepath, text):
        def removeMessage(se):
            self.messages.pop()
        self.messages.insert(0, (filepath, text))
        self.delay(6000, removeMessage)

    def pause(self, l= None):
        self.paused = True
    def resume(self, l=None):
        self.paused = False
    def exit(self, l=None):
        self.result = Result.EXIT
    def success(self, l=None):
        self.result = Result.SUCCESS

    def renderToWindow(self, window, surface):
        # render the surface with the entire world on it to the display,
        # handiling the fact that the world may be larger 
        # or smaller than the display
        # and that we never want to diplay the player closer than <pad>
        # to the edge of the screen
        # unless the player is closer than <pad> to the edge of the world
        window.display.fill((0,0,0))

        winWidth = window.display.get_width() 
        surfWidth = self.surface.get_width()
        winHeight = window.display.get_height() 
        surfHeight = self.surface.get_height()

        pad = 48

        if winWidth > surfWidth:
            self.winOffX = (winWidth-surfWidth)/2 
        else:
            if self.player.body.position.x + self.winOffX < pad:
                self.winOffX = pad - self.player.body.position.x 
            if self.player.body.position.x + self.winOffX > winWidth - pad:
                self.winOffX = winWidth - self.player.body.position.x - pad 
            self.winOffX = min(self.winOffX, 0)
            self.winOffX = max(self.winOffX, winWidth - surfWidth)

        if winHeight > surfHeight:
            self.winOffY = (winHeight-surfHeight)/2
        else:
            if self.player.body.position.y + self.winOffY < pad:
                self.winOffY = pad - self.player.body.position.y
            if self.player.body.position.y + self.winOffY > winHeight - pad:
                self.winOffY = winHeight - self.player.body.position.y - pad
            self.winOffY = min(self.winOffY, 0)
            self.winOffY = max(self.winOffY, winHeight - surfHeight)

        window.display.blit(self.surface,(self.winOffX, self.winOffY))

    def run(self, window):
        clock = pygame.time.Clock()

        hud = HUD()
        
        pauseMenu = Menu( "Game Paused", [
            ("Resume", self.resume ),
            ("Main Menu", self.exit),
            ("Restart", self.__init__), 
            ("Quit Game", pygame.quit )
            ])

        class Pause:
            def __init__(pself):
                pself.controls = {
                    pygame.K_ESCAPE: (self.pause, lambda x: x)
                }
            def available(self, level):
                return True
        pause = Pause()

        while True:
            if self.result != None:
                return (self.result, self.timer)

            self.surface.fill((0,0,0))

            for o in [self.background]+self.objects+[self.player, self.grid]:
                o.render(self.surface)

            for e in self.schedule:
                if e[0] < self.timer:
                    e[1](self)
                    self.schedule.remove(e)

            self.renderToWindow(window, self.surface)
            hud.render_controls(window, [o for o in self.objects if o.available(self)])
            hud.render_messages(window, self.messages)
            if self.paused:
                hud.render_menu(window, pauseMenu)
    
            #options = pymunk.pygame_util.DrawOptions(window.display)
            #self.space.debug_draw(options)

            pygame.display.flip()

            clock.tick(30) 

            if not self.paused:
                self.timer += 1000/30
                self.space.step(0.02) 
        
                for o in self.objects + [self.player]:
                    o.tick(self)
            
                self.input.get(window, self.objects + [self.player, pause])
            else:
                self.input.get(window, [pauseMenu], hud.menu_bounding_boxes(window, pauseMenu))
