
class Toggle():
    def __init__(self, *actions):
        self.index = 0;
        self.actions = actions

    def wrap(self, action):
        def wrapper(*args):
            self.index = (self.index + 1) % len(self.actions)
            action(*args)
        return wrapper

    def __len__(self): 
        return 3

    def __getitem__(self, key):
        if key == 0:
            return self.wrap(self.actions[self.index][0])
        elif key == 1:
            return (lambda x: x)
        elif key == 2:
            return self.actions[self.index][1]
        else:
            raise IndexError()
