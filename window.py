import pygame

class Window():
    def __init__(self, width, height):
        self.display = pygame.display.set_mode((width, height), pygame.RESIZABLE|pygame.HWSURFACE|pygame.DOUBLEBUF)

    def handle(self, event):
        if event.type == pygame.QUIT:
            pygame.quit()
        if event.type == pygame.VIDEORESIZE:
            self.display = pygame.display.set_mode((event.w, event.h), pygame.RESIZABLE|pygame.HWSURFACE|pygame.DOUBLEBUF)
            self.display.fill((0,0,0))
