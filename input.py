import pygame

class Input():
    def __init__(self, context):
        self.context = context
    def get(self, window, objects, boundingBoxes=[]):
        for event in pygame.event.get():
            window.handle(event)
            for o in objects:
                if o.available(self.context):
                    if event.type == pygame.KEYDOWN:
                        if event.key in o.controls:
                            o.controls[event.key][0](self.context)
                    if event.type == pygame.KEYUP:
                        if event.key in o.controls:
                            o.controls[event.key][1](self.context)
            if event.type in [pygame.MOUSEBUTTONDOWN, pygame.MOUSEMOTION]:
                for bb in boundingBoxes:
                    x = event.pos[0]
                    y = event.pos[1]
                    tl = bb.bb.tl
                    br = bb.bb.br
                    if x > tl[0] and x < br[0] and y > tl[1] and y < br[1]:
                        if event.type == pygame.MOUSEMOTION:
                            bb.onhover(self.context)
                        elif event.type == pygame.MOUSEBUTTONDOWN:
                            if event.button == bb.key:
                                bb.onclick(self.context)
